# [Cliptionary](./docs/FinalPresentation_RahulDamineni_Cliptionary.pptx)

Cliptionary is an app designed for contextual learning of new words. Growing up, I credit a substantial portion of my spoken English proficiency to the American TV series and movies I binged during college. This app aims to make that learning process more streamlined and effective.

**Example:** Imagine you're unfamiliar with the word `condescending`. You input it into the app, and it presents several YouTube videos with subtitles containing the word `condescending`. You can then narrow down the results to only include TV series or movies you've watched, allowing you to contextualize the clip and form a more vivid memory to aid recall.
![Quick demo](./docs/1sCliptionary_Demo.png)

## Why did I rewrite this app?
After completing [David Beazley's course](http://www.dabeaz.com/advprog.html), I felt compelled to apply the lessons I'd learned. Balancing design decisions while developing a novel and experimental product (a forecasting tool enhanced by dynamic exogenous context, a tale for another day) became a challenge, often leading to "decision fatigue." I opted to revisit and "refactor" an existing product with clearly defined requirements. This revision provided me ample room to delve into ideas such as layering, data abstraction, OOP, achieving the right balance between cohesion and decoupling, and software testing. While there were many other aspects I wanted to explore, I concluded this was a suitable juncture to pause, reserving the fine-tuning and performance guarantee setting for a future endeavor.

## How to get started? 
1. Please ignore the frontend folder; it merely illustrates the intended functionality. Focus exclusively on the backend.
2. Familiarize yourself with [my report](./docs/FinalReport_RahulDamineni_Cliptionary.pdf) for a comprehensive overview of the features.
3. Examine the [dependency graph](./docs/Backend-svc-dependency-graph.jpg) to understand how features are modularized into services.
4. Review the [API documentation](./docs/Backend%20API%20Documentation.docx) for a thorough understanding of the API.
5. Delve into the code. Begin with [main.py](./backend/app/main.py), followed by [routers](./backend/app/routers), [services](./backend/app/services/), and [models](./backend/app/models/)—these constitute the primary layers of abstraction in this implementation.

#### Feedback and code reviews are always appreciated!
