import pytest
from app.services.youtube import YoutubeService
from app.schemas import YTSearchResult, YTTranscriptSegment
from app.config import YOUTUBE_API_KEY


@pytest.fixture
def youtube_service_live():
    return YoutubeService(YOUTUBE_API_KEY)


def test_search_videos_live(youtube_service_live):
    user_query = "example query"

    results = youtube_service_live.search_videos(user_query, 5)

    assert results, "No YT search results found!"
    # Assert the output format is as expected
    for result in results:
        assert isinstance(result, YTSearchResult)
        assert hasattr(result, "videoId") and result.videoId
        assert hasattr(result, "thumbnailUrl") and result.thumbnailUrl
        assert hasattr(result, "title") and result.title


def test_fetch_closed_captions_live(youtube_service_live):
    # For this test, you need to know a video ID for which English captions are available.
    # Replace 'test_video_id' with a valid video ID for the test.
    test_video_id = "4C5krfpcj2o"

    cc_segments = youtube_service_live.fetch_closed_captions(test_video_id, "en")

    assert cc_segments, "No CC segments found!"
    # Assert the output format is as expected
    for segment in cc_segments:
        assert isinstance(segment, YTTranscriptSegment)
        assert hasattr(segment, "start")
        assert hasattr(segment, "duration")
        assert hasattr(segment, "text")
