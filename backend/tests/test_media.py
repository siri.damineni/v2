from app.services.media import MediaService
from app.models.media import Video, ClosedCaption
from app.schemas import YTSearchResult
from app.config import Languages


def test_init(db_session, youtube_service):
    media_service = MediaService(db_session, youtube_service)
    assert media_service.session == db_session
    assert media_service.youtube_service == youtube_service


def test_get_or_create_video(db_session, youtube_service):
    media_service = MediaService(db_session, youtube_service)
    yt_search_result = YTSearchResult(
        videoId="testVideoId", thumbnailUrl="testThumbnailUrl", title="testTitle"
    )
    video = media_service.get_or_create_video(yt_search_result)
    assert isinstance(video, Video)
    assert video.video_id == yt_search_result.videoId


def test_download_closed_caption(db_session, youtube_service):
    media_service = MediaService(db_session, youtube_service)
    yt_search_result = YTSearchResult(
        videoId="testVideoId", thumbnailUrl="testThumbnailUrl", title="testTitle"
    )
    video = media_service.get_or_create_video(yt_search_result)
    closed_caption = media_service.download_closed_caption(video, Languages.ENGLISH)
    assert isinstance(closed_caption, ClosedCaption)
    assert closed_caption.video_id == video.video_id
