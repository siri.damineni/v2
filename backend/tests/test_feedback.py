import pytest
from app.services.feedback import FeedbackService
from app.models.user import User, UserSearchResult, UserFavoriteContentSource
from sqlalchemy.orm import Session


def test_like_search_result(
    db_session,
    current_user,
    search_result,
    feedback_service,
):
    feedback_service.like_search_result(search_result.id)
    # assert that the search result is now liked by the current user
    user_search_result = (
        db_session.query(UserSearchResult)
        .filter(
            UserSearchResult.user_id == current_user.id,
            UserSearchResult.search_result_id == search_result.id,
        )
        .first()
    )

    assert user_search_result is not None
    assert user_search_result.is_liked


def test_unlike_search_result(
    db_session,
    current_user,
    search_result,
    feedback_service,
):
    # set the search result as liked initially
    user_search_result = UserSearchResult(
        user_id=current_user.id, search_result_id=search_result.id, is_liked=True
    )
    db_session.add(user_search_result)
    db_session.commit()

    feedback_service.unlike_search_result(search_result.id)
    # re-fetch the user_search_result
    user_search_result = (
        db_session.query(UserSearchResult)
        .filter(
            UserSearchResult.user_id == current_user.id,
            UserSearchResult.search_result_id == search_result.id,
        )
        .first()
    )

    assert user_search_result is not None
    assert not user_search_result.is_liked


def test_favorite_content_source(
    db_session,
    current_user,
    content_source,
    feedback_service,
):
    feedback_service.favorite_content_source(content_source.id)
    # assert that the content source is now favorited by the current user
    user_content_source = (
        db_session.query(UserFavoriteContentSource)
        .filter(
            UserFavoriteContentSource.user_id == current_user.id,
            UserFavoriteContentSource.content_source_id == content_source.id,
        )
        .first()
    )

    assert user_content_source is not None
    assert user_content_source.content_source_id == content_source.id


def test_unfavorite_content_source(
    db_session,
    current_user,
    content_source,
    feedback_service,
):
    # set the content source as favorited initially
    user_content_source = UserFavoriteContentSource(
        user_id=current_user.id,
        content_source_id=content_source.id,
    )
    db_session.add(user_content_source)
    db_session.commit()

    feedback_service.unfavorite_content_source(content_source.id)
    # re-fetch the user_content_source
    user_content_source = (
        db_session.query(UserFavoriteContentSource)
        .filter(
            UserFavoriteContentSource.user_id == current_user.id,
            UserFavoriteContentSource.content_source_id == content_source.id,
        )
        .first()
    )

    assert user_content_source is None
