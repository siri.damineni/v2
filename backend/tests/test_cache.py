import pytest
from app.services.cache import CacheService
from app.models.search import SearchResult
from app.models.media import Video, ClosedCaptionSegment, ClosedCaption
from app.config import Languages


@pytest.fixture
def cache_service(db_session):
    return CacheService(db_session)


def test_get_cached_results(cache_service, db_session):
    # Given a query that has been searched before
    query = "test query"

    video = Video(
        # id=1,
        video_id="4C5krfpcj2o",
        title="Test Title",
        thumbnail_url="http://test_thumbnail_url.com",
        # content_source_id=1,
    )

    cc_segment = ClosedCaptionSegment(
        # id=1,
        # closed_caption_id=1,
        start_time=10.0,
        end_time=15.0,
        text="Test Transcript Segment",
    )
    db_session.add(
        SearchResult(
            query=query,
            video=video,
            closed_caption_segment=cc_segment,
        )
    )
    db_session.commit()

    # When the get_cached_results method is called
    results = cache_service.get_cached_results(query)

    # Then the cached search result for the query is returned
    assert len(results) == 1
    assert results[0].query == query
    assert results[0].video_id == video.id
    assert results[0].closed_caption_segment_id == cc_segment.id


def test_cache_search_result(cache_service, db_session):
    # Given a query and a closed caption segment that has not been searched before
    query = "New query"

    # First commit VIDEO and CLOSED_CAPTION_SEGMENT instances to the database
    video = Video(
        # id=1,
        video_id="4C5krfpcj2o",
        title="Test Title",
        thumbnail_url="http://test_thumbnail_url.com",
        # content_source_id=1,
    )

    cc = ClosedCaption(
        language=Languages.ENGLISH,
        video=video,
    )

    cc_segment = ClosedCaptionSegment(
        # id=1,
        # closed_caption_id=1,
        start_time=10.0,
        end_time=15.0,
        text="Test Transcript Segment",
        closed_caption=cc,
    )

    db_session.add(video)
    db_session.add(cc)
    db_session.add(cc_segment)
    db_session.commit()

    # Then call cache_search_result method
    cache_service.cache_search_result(
        query=query,
        cc_segment=cc_segment,
        video=video,
    )

    # Then a new SearchResult is saved in the database
    result = cache_service.session.query(SearchResult).filter_by(query=query).first()
    assert result is not None
    assert result.query == query
    assert result.video_id == video.id
    assert result.closed_caption_segment_id == cc_segment.id
