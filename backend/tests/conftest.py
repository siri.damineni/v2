import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, configure_mappers, Session

from app.models.base import Base
from app.models.media import Video, ClosedCaption, ClosedCaptionSegment, ContentSource
from app.models.search import SearchResult
from app.models.user import User
from app.schemas import YTSearchResult, YTTranscriptSegment
from app.services.feedback import FeedbackService
from app.config import DBConfig, Languages

from unittest.mock import MagicMock

DATABASE_URL = DBConfig().construct_db_url(test=True)

engine = create_engine(DATABASE_URL)
configure_mappers()
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture
def db_session() -> Session:
    Base.metadata.create_all(bind=engine)
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()
        Base.metadata.drop_all(bind=engine)


@pytest.fixture
def current_user(db_session: Session):
    user = User(email="john@test.com", hashed_password="hashed_password")
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def search_result(db_session: Session) -> SearchResult:
    query = "test query"

    # First commit VIDEO and CLOSED_CAPTION_SEGMENT instances to the database
    video = Video(
        # id=1,
        video_id="4C5krfpcj2o",
        title="Test Title",
        thumbnail_url="http://test_thumbnail_url.com",
        # content_source_id=1,
    )

    cc = ClosedCaption(
        language=Languages.ENGLISH,
        video=video,
    )

    cc_segment = ClosedCaptionSegment(
        # id=1,
        # closed_caption_id=1,
        start_time=10.0,
        end_time=15.0,
        text="Test Transcript Segment",
        closed_caption=cc,
    )

    search_result = SearchResult(
        query=query,
        closed_caption_segment=cc_segment,
        video=video,
    )

    db_session.add(cc)
    db_session.add(search_result)
    db_session.commit()

    return search_result


@pytest.fixture
def content_source(db_session: Session, search_result: SearchResult):
    # set up a ContentSource in the database
    cs = ContentSource(name="YouTube", video=[search_result.video])
    db_session.add(cs)
    db_session.commit()
    return cs


@pytest.fixture
def feedback_service(db_session: Session, current_user):
    return FeedbackService(db_session, current_user)


@pytest.fixture
def youtube_service():
    # create a mock YoutubeService
    youtube_service_mock = MagicMock()

    # set the return values for the mock methods
    youtube_service_mock.search_videos.return_value = [
        YTSearchResult(
            videoId="test1",
            title="Test Video 1",
            thumbnailUrl="http://example.com/1.jpg",
        ),
        YTSearchResult(
            videoId="test2",
            title="Test Video 2",
            thumbnailUrl="http://example.com/2.jpg",
        ),
    ]
    youtube_service_mock.fetch_closed_captions.return_value = [
        YTTranscriptSegment(start=0, duration=10, text="this is a test"),
        YTTranscriptSegment(start=10, duration=10, text="this is another test"),
    ]

    return youtube_service_mock
