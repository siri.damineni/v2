import pytest
from sqlalchemy.orm import Session
from app.services.user import UserService
from app.services.feedback import FeedbackService
from app.models.user import (
    User,
    UserFollow,
    UserBlock,
    UserPreference,
    UserSearchResult,
    UserFavoriteContentSource,
)


# First, let's set up some fixtures to use in the tests
@pytest.fixture
def following_user(db_session: Session):
    user = User(email="following@test.com", hashed_password="hashed_password")
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def blocked_user(db_session: Session):
    user = User(email="blocked@test.com", hashed_password="hashed_password")
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture
def user_service(db_session: Session, feedback_service, current_user):
    return UserService(db_session, feedback_service, current_user)


# Now we can write the tests
def test_follow_user(db_session, user_service, current_user, following_user):
    user_service.follow_user(following_user.id)
    follow_relationship = (
        db_session.query(UserFollow)
        .filter(
            UserFollow.follower_id == current_user.id,
            UserFollow.following_id == following_user.id,
        )
        .first()
    )
    assert follow_relationship is not None


def test_unfollow_user(db_session, user_service, current_user, following_user):
    user_follow = UserFollow(
        follower_id=current_user.id, following_id=following_user.id
    )
    db_session.add(user_follow)
    db_session.commit()

    user_service.unfollow_user(following_user.id)
    follow_relationship = (
        db_session.query(UserFollow)
        .filter(
            UserFollow.follower_id == current_user.id,
            UserFollow.following_id == following_user.id,
        )
        .first()
    )
    assert follow_relationship is None


def test_get_user_followers(db_session, current_user, user_service, following_user):
    """
    Think of this as current user being followed by some other user (following_user)
    And so, we should see current user in the list of followers of `following_user`
    """
    feedback_service = FeedbackService(session=db_session, current_user=following_user)
    following_user_service = UserService(
        db_session, feedback_service, current_user=following_user
    )
    following_user_service.follow_user(current_user.id)

    followers = user_service.get_user_followers()
    assert len(followers)
    assert any(follower.id == following_user.id for follower in followers)


def test_get_user_followings(user_service, current_user, following_user):
    user_service.follow_user(following_user.id)
    user_service.session.commit()

    followings = user_service.get_user_followings()
    assert followings is not None
    assert any(following.id == following_user.id for following in followings)


def test_block_user(db_session, user_service, current_user, blocked_user):
    user_service.block_user(blocked_user.id)
    block_relationship = (
        db_session.query(UserBlock)
        .filter(
            UserBlock.blocker_id == current_user.id,
            UserBlock.blocked_id == blocked_user.id,
        )
        .first()
    )
    assert block_relationship is not None


def test_unblock_user(db_session, user_service, current_user, blocked_user):
    user_block = UserBlock(blocker_id=current_user.id, blocked_id=blocked_user.id)
    db_session.add(user_block)
    db_session.commit()

    user_service.unblock_user(blocked_user.id)
    block_relationship = (
        db_session.query(UserBlock)
        .filter(
            UserBlock.blocker_id == current_user.id,
            UserBlock.blocked_id == blocked_user.id,
        )
        .first()
    )
    assert block_relationship is None


def test_get_user_blocked_users(db_session, user_service, current_user, blocked_user):
    user_block = UserBlock(blocker_id=current_user.id, blocked_id=blocked_user.id)
    db_session.add(user_block)
    db_session.commit()

    blocked_users = user_service.get_user_blocked_users()
    assert blocked_users is not None
    assert any(blocked.id == blocked_user.id for blocked in blocked_users)


def test_get_user_statistics(user_service, current_user):
    user_statistics = user_service.get_user_statistics()
    assert user_statistics.user.id == current_user.id
    assert (
        user_statistics.total_queries
        == user_statistics.continuous_day_streak
        == user_statistics.videos_watched
        == user_statistics.words_learned
        == 0
    )


def test_get_user_preferences(user_service, current_user):
    user_preferences = user_service.get_user_preferences()
    assert user_preferences.user.id == current_user.id


def test_update_user_preferences(db_session, user_service, current_user):
    theme = "dark"
    user_service.update_user_preferences(theme)
    updated_preferences = (
        db_session.query(UserPreference)
        .filter(UserPreference.user_id == current_user.id)
        .first()
    )
    assert updated_preferences.theme == theme


def test_like_search_result(db_session, user_service, current_user, search_result):
    user_service.like_search_result(search_result.id)
    liked_result = (
        db_session.query(UserSearchResult)
        .filter(
            UserSearchResult.user_id == current_user.id,
            UserSearchResult.search_result_id == search_result.id,
            UserSearchResult.is_liked == True,
        )
        .first()
    )
    assert liked_result is not None


def test_unlike_search_result(db_session, user_service, current_user, search_result):
    user_service.like_search_result(search_result.id)
    user_service.unlike_search_result(search_result.id)
    unliked_result = (
        db_session.query(UserSearchResult)
        .filter(
            UserSearchResult.user_id == current_user.id,
            UserSearchResult.search_result_id == search_result.id,
            UserSearchResult.is_liked == False,
        )
        .first()
    )
    assert unliked_result is not None


def test_favorite_content_source(
    db_session, user_service, current_user, content_source
):
    user_service.favorite_content_source(content_source.id)
    favorited_source = (
        db_session.query(UserFavoriteContentSource)
        .filter(
            UserFavoriteContentSource.user_id == current_user.id,
            UserFavoriteContentSource.content_source_id == content_source.id,
        )
        .first()
    )
    assert favorited_source is not None


def test_unfavorite_content_source(
    db_session, user_service, current_user, content_source
):
    user_service.favorite_content_source(content_source.id)
    user_service.unfavorite_content_source(content_source.id)
    unfavorited_source = (
        db_session.query(UserFavoriteContentSource)
        .filter(
            UserFavoriteContentSource.user_id == current_user.id,
            UserFavoriteContentSource.content_source_id == content_source.id,
        )
        .first()
    )
    assert unfavorited_source is None
