import pytest
from app.services.media import MediaService
from app.services.cache import CacheService
from app.services.search import SearchService
from app.models.media import ClosedCaption, ClosedCaptionSegment, Video, ContentSource
from app.config import Languages


class TestSearchService:
    @pytest.fixture
    def media_service(
        self,
        youtube_service,
        db_session,
    ):
        return MediaService(db_session, youtube_service)

    @pytest.fixture
    def cache_service(self, db_session):
        return CacheService(db_session)

    @pytest.fixture
    def search_service(
        self,
        db_session,
        youtube_service,
        media_service,
        cache_service,
    ):
        return SearchService(db_session, youtube_service, media_service, cache_service)

    def test_search_videos(self, search_service):
        # now call the method under test
        results = search_service.search_videos("test", Languages.ENGLISH)

        # assert the results
        assert len(results) == 2

    def test_get_matches_in_cc(self, search_service, db_session):
        # Given a query and a closed caption with matching segments
        query = "test"
        cc_segment = ClosedCaptionSegment(
            start_time=0, end_time=10, text="this is a test"
        )
        cc = ClosedCaption(closed_caption_segments=[cc_segment])

        db_session.add(cc)
        db_session.commit()

        # When _get_matches_in_cc is called
        matches = search_service._get_matches_in_cc(query, cc)

        # Then the matching closed caption segment is returned
        assert len(matches) == 1
        assert matches[0].text == cc_segment.text

    def test_get_cc_matches(self, search_service, db_session):
        # Given a query and a closed caption with matching segments in the database
        query = "test"
        content_source = ContentSource(name="YOUTUBE")
        video = Video(
            # id=1,
            video_id="4C5krfpcj2o",
            title="Test Title",
            thumbnail_url="http://test_thumbnail_url.com",
            content_source=content_source,
        )

        cc_segment = ClosedCaptionSegment(
            # id=1,
            # closed_caption_id=1,
            start_time=10.0,
            end_time=15.0,
            text="test Transcript Segment",
        )
        # db_session.add(cc_segment)
        db_session.add(
            ClosedCaption(
                language=Languages.ENGLISH,
                video=video,
                closed_caption_segments=[cc_segment],
            )
        )
        db_session.commit()

        # When get_cc_matches is called
        matches = search_service.get_cc_matches(query)

        # Then the matching closed caption segment is returned
        assert len(matches) == 1
        assert matches[0].text == cc_segment.text
