import pytest

from sqlalchemy.orm import Session
from app.services.analytics import AnalyticsService
from app.models.user import User, UserSearchResult, UserQuery


@pytest.fixture
def analytics_service(db_session: Session, current_user: User):
    return AnalyticsService(db_session, current_user)


def test_log_search_result_play(
    db_session, current_user, search_result, analytics_service
):
    analytics_service.log_search_result_play(search_result.id)
    user_search_result = (
        db_session.query(UserSearchResult)
        .filter(
            UserSearchResult.user_id == current_user.id,
            UserSearchResult.search_result_id == search_result.id,
        )
        .first()
    )
    assert user_search_result.times_matched == 1
    assert user_search_result.times_played == 1


def test_log_search_result_match(
    db_session, current_user, search_result, analytics_service
):
    analytics_service.log_search_result_match(search_result.id)
    user_search_result = (
        db_session.query(UserSearchResult)
        .filter(
            UserSearchResult.user_id == current_user.id,
            UserSearchResult.search_result_id == search_result.id,
        )
        .first()
    )
    assert user_search_result.times_matched == 1


def test_get_most_popular_query_of_the_day(
    db_session,
    current_user,
    analytics_service,
):
    # Assume that the following queries are executed by the user today
    for query in ["test query", "test query", "another query"]:
        user_query = UserQuery(user_id=current_user.id, query=query)
        db_session.add(user_query)
        db_session.commit()

    most_popular_query = analytics_service.get_most_popular_query_of_the_day()
    assert most_popular_query == "test query"


def test_count_query_usage(
    db_session,
    current_user,
    analytics_service,
):
    # Assume that the user executes the following queries
    for _ in range(5):
        user_query = UserQuery(user_id=current_user.id, query="test query")
        db_session.add(user_query)
    db_session.commit()

    query_usage_count = analytics_service.count_query_usage("test query")
    assert query_usage_count == 5


def test_count_video_views(analytics_service, search_result):
    analytics_service.log_search_result_play(search_result.id)
    assert analytics_service.count_video_views(search_result.video.id) == 1
