import jwt
import pytest
import app.exceptions as exc
from app.services.authentication import AuthService
from app.models.user import User
from app.schemas import UserInCreate, Token
from app.config import settings
from datetime import timedelta


@pytest.fixture
def auth_service(db_session):
    return AuthService(db_session)


def test_create_access_token(auth_service):
    token = auth_service.create_access_token(
        subject="test", expires_delta=timedelta(minutes=15)
    )

    assert token
    assert isinstance(token, Token)
    assert isinstance(token.access_token, str)


def test_get_password_hash(auth_service):
    password = "mysecretpassword"
    hashed_password = auth_service.get_password_hash(password)

    assert hashed_password
    assert isinstance(hashed_password, str)
    assert hashed_password != password


def test_verify_password(auth_service):
    password = "mysecretpassword"
    hashed_password = auth_service.get_password_hash(password)

    assert auth_service.verify_password(password, hashed_password)
    assert not auth_service.verify_password("wrongpassword", hashed_password)


def test_create_user(auth_service):
    user_in_create = UserInCreate(
        email="test_create@test.com", password="mysecretpassword"
    )
    token = auth_service.create_user(user_in_create)

    assert isinstance(token, Token)

    # Verify token contents
    payload = jwt.decode(
        token.access_token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
    )
    assert payload
    assert "sub" in payload
    assert payload["sub"] == "test_create@test.com"

    with pytest.raises(exc.UserAlreadyExistsError):
        auth_service.create_user(user_in_create)


def test_authenticate_user(auth_service, db_session):
    user_in_create = UserInCreate(
        email="test_auth@test.com", password="mysecretpassword"
    )
    auth_service.create_user(user_in_create)

    token = auth_service.authenticate_user("test_auth@test.com", "mysecretpassword")
    assert isinstance(token, Token)

    # Verify token contents
    payload = jwt.decode(
        token.access_token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
    )
    assert payload
    assert "sub" in payload
    assert payload["sub"] == "test_auth@test.com"

    with pytest.raises(exc.InvalidCredentialsError):
        auth_service.authenticate_user("test_auth@test.com", "wrongpassword")


def test_get_user_by_email(auth_service, db_session):
    # Assuming USER_JOHN was added to db_session before
    user = User(email="john@test.com", hashed_password="hashed_password")
    db_session.add(user)
    db_session.commit()

    retrieved_user = auth_service.get_user_by_email("john@test.com")
    assert retrieved_user.email == "john@test.com"

    with pytest.raises(exc.UserNotFoundError):
        auth_service.get_user_by_email("nonexistent@test.com")
