import os
import ast
import inspect
from typing import List

# Function to extract locally defined classes and their methods from Python file
def extract_classes_and_methods(file_path):
    classes = []
    with open(file_path, "r") as f:
        tree = ast.parse(f.read(), file_path)
        for node in ast.walk(tree):
            if isinstance(node, ast.ClassDef):
                class_name = node.name
                methods = []
                class_locals = {}
                exec(compile(tree, file_path, "exec"), class_locals)
                cls = class_locals[class_name]
                for n in node.body:
                    if isinstance(n, ast.FunctionDef):
                        method_name = n.name
                        args = inspect.signature(getattr(cls, method_name)).parameters
                        args_list = [
                            f"{arg}: {args[arg].annotation}"
                            if args[arg].annotation != inspect.Parameter.empty
                            else arg
                            for arg in args
                        ]
                        return_type = getattr(cls, method_name).__annotations__.get(
                            "return", None
                        )
                        method_signature = (
                            f"{method_name}({', '.join(args_list)}) -> {return_type}"
                        )
                        methods.append(method_signature)
                classes.append((class_name, methods))
    return classes


# Specify the directory path
folder_path = "app/routers"

# Iterate through the directory and its subdirectories
for root, dirs, files in os.walk(folder_path):
    for file in files:
        if file.endswith(".py"):
            file_path = os.path.join(root, file)
            classes_and_methods = extract_classes_and_methods(file_path)
            for class_name, methods in classes_and_methods:
                print(f"File: {file_path}")
                print(f"Class: {class_name}")
                print(f"Methods:")
                for method in methods:
                    print(f"- {method}")
                print()
