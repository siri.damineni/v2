import os
from sqlalchemy import create_engine
from sqlalchemy.orm import configure_mappers

from sqlalchemy.orm import sessionmaker
from app.config import DATABASE_URL

engine = create_engine(DATABASE_URL)
configure_mappers()
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_session():
    # from app.models.base import Base
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
        # Base.metadata.drop_all(bind=engine)
