from typing import List
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request
from starlette.types import ASGIApp
from .services.authentication import AuthService


class AuthenticationError(Exception):
    ...


class AuthenticationMiddleware(BaseHTTPMiddleware):
    def __init__(
        self,
        app: ASGIApp,
        auth_service: AuthService,
        exclusion_paths: List[str] = None,
    ) -> None:
        super().__init__(app)
        self.auth_service = auth_service
        self.exclusion_paths = exclusion_paths or []

    async def dispatch(self, request: Request, call_next):
        if any(request.url.path.startswith(path) for path in self.exclusion_paths):
            return await call_next(request)

        token = request.headers.get("Authorization")
        if token:
            token = token.split(" ")[-1]
            request.state.current_user_email = self.auth_service.get_user_from_token(
                token
            )

        response = await call_next(request)
        return response
