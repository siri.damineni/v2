import os
from enum import Enum
from typing import Any
from urllib.parse import quote_plus
from pydantic_settings import BaseSettings
from dotenv import load_dotenv

load_dotenv()

YOUTUBE_API_KEY = os.getenv("YOUTUBE_API_KEY")


class Settings(BaseSettings):
    SECRET_KEY: str = os.getenv("JWT_SECRET")
    ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30
    MAX_SEARCH_RESULTS: int = 20
    MAX_YT_VIDEOS_RESULTS: int = 10


class Languages(Enum):
    """
    (TRANSLATION LANGUAGES)
    - af ("Afrikaans")
    - ak ("Akan")
    - sq ("Albanian")
    - am ("Amharic")
    - ar ("Arabic")
    - hy ("Armenian")
    - as ("Assamese")
    - ay ("Aymara")
    - az ("Azerbaijani")
    - bn ("Bangla")
    - eu ("Basque")
    - be ("Belarusian")
    - bho ("Bhojpuri")
    - bs ("Bosnian")
    - bg ("Bulgarian")
    - my ("Burmese")
    - ca ("Catalan")
    - ceb ("Cebuano")
    - zh-Hans ("Chinese (Simplified)")
    - zh-Hant ("Chinese (Traditional)")
    - co ("Corsican")
    - hr ("Croatian")
    - cs ("Czech")
    - da ("Danish")
    - dv ("Divehi")
    - nl ("Dutch")
    - en ("English")
    - eo ("Esperanto")
    - et ("Estonian")
    - ee ("Ewe")
    - fil ("Filipino")
    - fi ("Finnish")
    - fr ("French")
    - gl ("Galician")
    - lg ("Ganda")
    - ka ("Georgian")
    - de ("German")
    - el ("Greek")
    - gn ("Guarani")
    - gu ("Gujarati")
    - ht ("Haitian Creole")
    - ha ("Hausa")
    - haw ("Hawaiian")
    - iw ("Hebrew")
    - hi ("Hindi")
    - hmn ("Hmong")
    - hu ("Hungarian")
    - is ("Icelandic")
    - ig ("Igbo")
    - id ("Indonesian")
    - ga ("Irish")
    - it ("Italian")
    - ja ("Japanese")
    - jv ("Javanese")
    - kn ("Kannada")
    - kk ("Kazakh")
    - km ("Khmer")
    - rw ("Kinyarwanda")
    - ko ("Korean")
    - kri ("Krio")
    - ku ("Kurdish")
    - ky ("Kyrgyz")
    - lo ("Lao")
    - la ("Latin")
    - lv ("Latvian")
    - ln ("Lingala")
    - lt ("Lithuanian")
    - lb ("Luxembourgish")
    - mk ("Macedonian")
    - mg ("Malagasy")
    - ms ("Malay")
    - ml ("Malayalam")
    - mt ("Maltese")
    - mi ("Māori")
    - mr ("Marathi")
    - mn ("Mongolian")
    - ne ("Nepali")
    - nso ("Northern Sotho")
    - no ("Norwegian")
    - ny ("Nyanja")
    - or ("Odia")
    - om ("Oromo")
    - ps ("Pashto")
    - fa ("Persian")
    - pl ("Polish")
    - pt ("Portuguese")
    - pa ("Punjabi")
    - qu ("Quechua")
    - ro ("Romanian")
    - ru ("Russian")
    - sm ("Samoan")
    - sa ("Sanskrit")
    - gd ("Scottish Gaelic")
    - sr ("Serbian")
    - sn ("Shona")
    - sd ("Sindhi")
    - si ("Sinhala")
    - sk ("Slovak")
    - sl ("Slovenian")
    - so ("Somali")
    - st ("Southern Sotho")
    - es ("Spanish")
    - su ("Sundanese")
    - sw ("Swahili")
    - sv ("Swedish")
    - tg ("Tajik")
    - ta ("Tamil")
    - tt ("Tatar")
    - te ("Telugu")
    - th ("Thai")
    - ti ("Tigrinya")
    - ts ("Tsonga")
    - tr ("Turkish")
    - tk ("Turkmen")
    - uk ("Ukrainian")
    - ur ("Urdu")
    - ug ("Uyghur")
    - uz ("Uzbek")
    - vi ("Vietnamese")
    - cy ("Welsh")
    - fy ("Western Frisian")
    - xh ("Xhosa")
    - yi ("Yiddish")
    - yo ("Yoruba")
    - zu ("Zulu")
    """

    ENGLISH = "en"
    SPANISH = "es"
    FRENCH = "fr"
    GERMAN = "de"
    # Add more languages as needed


class DBConfig:
    # Add the following:
    DATABASE_NAME = os.getenv("DATABASE_NAME")
    DATABASE_USER = os.getenv("DATABASE_USER")
    DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD")
    DATABASE_HOST = os.getenv("DATABASE_HOST")
    DATABASE_PORT = os.getenv("DATABASE_PORT")

    def construct_db_url(self, test=False) -> str:
        encoded_password = quote_plus(self.DATABASE_PASSWORD)

        if test:
            return f"postgresql://{self.DATABASE_USER}:{encoded_password}@{self.DATABASE_HOST}:{self.DATABASE_PORT}/{self.DATABASE_NAME}_test"
        return f"postgresql://{self.DATABASE_USER}:{encoded_password}@{self.DATABASE_HOST}:{self.DATABASE_PORT}/{self.DATABASE_NAME}"


settings = Settings()
DATABASE_URL = DBConfig().construct_db_url()
