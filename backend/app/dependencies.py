from fastapi import Depends, Request

from .services.user import UserService
from .services.authentication import AuthService
from .services.media import MediaService
from .services.youtube import YoutubeService
from .services.feedback import FeedbackService
from .services.cache import CacheService
from .services.search import SearchService
from .services.analytics import AnalyticsService

from .database import get_session
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from .database import get_session
from .schemas import UserInResponse, Token
from . import exceptions as exc
from .config import YOUTUBE_API_KEY

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/login")


def get_auth_service(session: Session = Depends(get_session)) -> AuthService:
    return AuthService(session)


def get_current_user(
    request: Request,
    auth_service: AuthService = Depends(get_auth_service),
    token: Token = Depends(oauth2_scheme),
) -> UserInResponse:
    # Could be replaced with AuthService.get_current_user by
    # making the middleware pass-in the entire token instead of extracting current_user_email
    current_user_email = request.state.current_user_email or None
    if not current_user_email:
        raise exc.UnauthorizedError

    return auth_service.get_user_by_email(current_user_email)


def get_youtube_service() -> YoutubeService:
    return YoutubeService(api_key=YOUTUBE_API_KEY)


def get_cache_service(session: Session = Depends(get_session)) -> CacheService:
    return CacheService(session)


def get_media_service(
    session: Session = Depends(get_session),
    youtube_service: YoutubeService = Depends(get_youtube_service),
) -> MediaService:
    return MediaService(session, youtube_service)


def get_feedback_service(
    session: Session = Depends(get_session),
    current_user: str = Depends(get_current_user),
) -> FeedbackService:
    return FeedbackService(session, current_user)


def get_user_service(
    session: Session = Depends(get_session),
    feedback_service: FeedbackService = Depends(get_feedback_service),
    current_user: str = Depends(get_current_user),
) -> UserService:
    return UserService(session, feedback_service, current_user)


def get_analytics_service(
    session: Session = Depends(get_session),
    current_user: str = Depends(get_current_user),
) -> AnalyticsService:
    return AnalyticsService(session, current_user)


def get_search_service(
    session: Session = Depends(get_session),
    current_user: str = Depends(get_current_user),
    youtube_service: YoutubeService = Depends(get_youtube_service),
    cache_service: CacheService = Depends(get_cache_service),
    media_service: MediaService = Depends(get_media_service),
    analytics_service: AnalyticsService = Depends(get_analytics_service),
) -> SearchService:
    return SearchService(
        session,
        youtube_service,
        media_service,
        cache_service,
        analytics_service,
    )
