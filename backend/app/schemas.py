from pydantic import BaseModel, EmailStr
from typing import Optional
from datetime import datetime


class Token(BaseModel):
    access_token: str
    token_type: str


class UserInCreate(BaseModel):
    email: EmailStr
    password: str


class UserInResponse(BaseModel):
    id: int
    email: str

    class Config:
        orm_mode = True


class SearchResult(BaseModel):
    id: int
    query: str
    video_id: str
    thumbnail_url: str
    title: str
    start_time: Optional[int]
    end_time: Optional[int]
    text: Optional[str]

    class Config:
        orm_mode = True


class SearchHistory(BaseModel):
    query: str
    timestamp: datetime
    count: int


class YTSearchResult(BaseModel):
    videoId: str
    thumbnailUrl: str
    title: str


class YTTranscriptSegment(BaseModel):
    start: float
    duration: float
    text: str
