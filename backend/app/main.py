from .database import engine

from .models.base import Base
from . import models  # Required for creating tables using declarative base

Base.metadata.create_all(bind=engine)


from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse
from .database import get_session
from .middleware import AuthenticationMiddleware
from app.services.authentication import AuthService
from app.routers import analytics, authentication, feedback, search, users
from app import exceptions as exc

app = FastAPI()

# Add CORS middleware
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"],
)

# Add Authentication middleware
session = get_session()
auth_service = AuthService(session)
app.add_middleware(
    AuthenticationMiddleware,
    auth_service=auth_service,
    exclusion_paths=["/api/auth"],
)


app.include_router(authentication.router, prefix="/api", tags=["auth"])
app.include_router(users.router, prefix="/api", tags=["users"])
app.include_router(search.router, prefix="/api", tags=["search"])
app.include_router(feedback.router, prefix="/api", tags=["feedback"])
app.include_router(analytics.router, prefix="/api", tags=["analytics"])


# Add common exception handling


@app.exception_handler(exc.UnauthorizedError)
async def unauthorized_exception_handler(request, exc):
    return JSONResponse(
        status_code=401,
        content={"message": "Unauthorized."},
    )
