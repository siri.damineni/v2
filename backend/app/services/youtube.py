from googleapiclient.discovery import build
from youtube_transcript_api import YouTubeTranscriptApi, NoTranscriptFound
from app.schemas import YTSearchResult, YTTranscriptSegment
from typing import List


class YoutubeService:
    def __init__(self, api_key):
        self.api_key = api_key
        self.youtube = build("youtube", "v3", developerKey=self.api_key)

    def search_videos(self, query: str, max_results: int) -> List[YTSearchResult]:
        try:
            search_response = (
                self.youtube.search()
                .list(
                    q=query,
                    part="id,snippet",
                    maxResults=max_results,
                    type="video",
                    videoCaption="closedCaption",
                )
                .execute()
            )
            video_results = search_response.get("items", [])
            yt_search_results = [
                YTSearchResult(
                    videoId=vr["id"]["videoId"],
                    thumbnailUrl=vr["snippet"]["thumbnails"]["high"]["url"],
                    title=vr["snippet"]["title"],
                )
                for vr in video_results
            ]
        except Exception as e:
            print(f"Error searching videos: {e}")
            return []

        return yt_search_results

    def fetch_closed_captions(
        self, video_id: str, language: str = "en"
    ) -> List[YTTranscriptSegment]:
        transcript = YouTubeTranscriptApi.get_transcript(video_id, languages=[language])
        transcript = [
            YTTranscriptSegment(**transcript_segment)
            for transcript_segment in transcript
        ]
        return transcript
