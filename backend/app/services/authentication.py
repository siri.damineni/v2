import jwt
from jwt import PyJWTError
from datetime import timedelta, datetime
from typing import Any, Union, Optional
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from app import models, schemas, exceptions as exc
from app.config import settings


class AuthService:
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def __init__(self, session: Session):
        self.session = session

    def create_access_token(
        self,
        subject: Union[str, Any],
        expires_delta: timedelta = timedelta(minutes=15),
    ) -> str:
        to_encode = {"exp": datetime.utcnow() + expires_delta, "sub": str(subject)}
        encoded_jwt = jwt.encode(
            to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM
        )
        return schemas.Token(access_token=encoded_jwt, token_type="bearer")

    @classmethod
    def get_password_hash(cls, password: str) -> str:
        return cls.pwd_context.hash(password)

    @classmethod
    def verify_password(cls, plain_password: str, hashed_password: str) -> bool:
        return cls.pwd_context.verify(plain_password, hashed_password)

    def authenticate_user(
        self, email: str, password: str
    ) -> Union[schemas.Token, None]:
        user = self.get_user_by_email(email)
        if not self.verify_password(password, user.hashed_password):
            raise exc.InvalidCredentialsError
        return self.create_access_token(user.email)

    def get_user_by_email(self, email: str) -> Union[schemas.UserInResponse, None]:
        user = (
            self.session.query(models.User).filter(models.User.email == email).first()
        )
        if not user:
            raise exc.UserNotFoundError

        return user

    def create_user(self, user: schemas.UserInCreate) -> schemas.Token:
        try:
            self.get_user_by_email(user.email)
        except exc.UserNotFoundError:
            hashed_password = self.get_password_hash(user.password)
            db_user = models.User(email=user.email, hashed_password=hashed_password)
            self.session.add(db_user)
            self.session.commit()
            self.session.refresh(db_user)
            return self.create_access_token(db_user.email)

        raise exc.UserAlreadyExistsError

    def get_current_user(self, token: str) -> Optional[schemas.UserInResponse]:
        try:
            payload = jwt.decode(
                token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
            )
            email = payload.get("sub")
            if not email:
                return None
        except PyJWTError:
            return None

        user = self.get_user_by_email(email)

        return schemas.UserInResponse.from_orm(user)  # return user as Pydantic model

    def get_user_from_token(self, token: str) -> str:
        try:
            payload = jwt.decode(
                token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
            )
            email = payload.get("sub")
            if email is None:
                return None
        except PyJWTError:
            return None
        return email
