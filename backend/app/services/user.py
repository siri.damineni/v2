from sqlalchemy.orm import Session
from app.models.user import (
    User,
    UserFollow,
    UserBlock,
    UserStatistic,
    UserPreference,
    UserQuery,
)
from app.schemas import UserInResponse, SearchHistory
from app.services.feedback import FeedbackService
from typing import List


class UserService:
    def __init__(
        self,
        session: Session,
        feedback_service: FeedbackService,
        current_user: UserInResponse,
    ):
        self.session = session
        self.feedback_service = feedback_service
        self.current_user = current_user

    def follow_user(self, following_id: int):
        follower = self.current_user
        following = self.session.query(User).filter(User.id == following_id).first()
        if follower and following:
            follow = UserFollow(follower_id=follower.id, following_id=following.id)
            self.session.add(follow)
            self.session.commit()

    def unfollow_user(self, following_id: int):
        follower = self.current_user
        following = self.session.query(User).filter(User.id == following_id).first()
        if follower and following:
            follow = (
                self.session.query(UserFollow)
                .filter(
                    UserFollow.follower_id == follower.id,
                    UserFollow.following_id == following.id,
                )
                .first()
            )
            if follow:
                self.session.delete(follow)
                self.session.commit()

    def block_user(self, blocked_id: int):
        blocker = self.current_user
        blocked = self.session.query(User).filter(User.id == blocked_id).first()
        if blocker and blocked:
            block = UserBlock(blocker_id=blocker.id, blocked_id=blocked.id)
            self.session.add(block)
            self.session.commit()

    def unblock_user(self, blocked_id: int):
        blocker = self.current_user
        blocked = self.session.query(User).filter(User.id == blocked_id).first()
        if blocker and blocked:
            block = (
                self.session.query(UserBlock)
                .filter(
                    UserBlock.blocker_id == blocker.id,
                    UserBlock.blocked_id == blocked.id,
                )
                .first()
            )
            if block:
                self.session.delete(block)
                self.session.commit()

    def get_user_statistics(self):
        if not self.current_user.statistics:
            self.current_user.statistics = UserStatistic(user_id=self.current_user.id)
            self.session.add(self.current_user.statistics)
            self.session.commit()

        return self.current_user.statistics

    def get_user_preferences(self):
        preferences = self.current_user.preferences

        # Lazy initialization
        if preferences is None:
            preferences = UserPreference(user_id=self.current_user.id, theme="default")
            self.current_user.preferences = preferences
            self.session.add(preferences)

        return preferences

    def update_user_preferences(self, theme: str):
        preferences = self.get_user_preferences()
        preferences.theme = theme
        self.session.commit()

    def get_user_followers(self):
        followers = (
            self.session.query(UserFollow)
            .filter(UserFollow.following_id == self.current_user.id)
            .all()
        )
        return [follow.follower for follow in followers]

    def get_user_followings(self):
        followings = (
            self.session.query(UserFollow)
            .filter(UserFollow.follower_id == self.current_user.id)
            .all()
        )
        return [follow.following for follow in followings]

    def get_user_blocked_users(self):
        blocked_users = (
            self.session.query(UserBlock)
            .filter(UserBlock.blocker_id == self.current_user.id)
            .all()
        )
        return [block.blocked for block in blocked_users]

    def get_search_history(self) -> List[SearchHistory]:
        search_history = (
            self.session.query(UserQuery)
            .filter(UserQuery.user_id == self.current_user.id)
            .order_by(UserQuery.date.desc())
            .all()
        )

        return [
            SearchHistory(query=item.query, timestamp=item.date, count=item.count)
            for item in search_history
        ]

    def like_search_result(self, search_result_id: int):
        self.feedback_service.like_search_result(search_result_id)

    def unlike_search_result(self, search_result_id: int):
        self.feedback_service.unlike_search_result(search_result_id)

    def favorite_content_source(self, content_source_id: int):
        self.feedback_service.favorite_content_source(content_source_id)

    def unfavorite_content_source(self, content_source_id: int):
        self.feedback_service.unfavorite_content_source(content_source_id)
