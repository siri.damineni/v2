from sqlalchemy.orm import Session
from sqlalchemy.sql import and_, func
from app.models.user import User, UserSearchResult, UserStatistic, UserQuery
from app.schemas import UserInResponse
from app.models.search import SearchResult
import app.exceptions as exc


class AnalyticsService:
    def __init__(self, session: Session, current_user: str):
        self.session = session
        self.current_user = current_user

    def log_user_query(self, query: str):
        user_id = self.current_user.id
        user_query = (
            self.session.query(UserQuery)
            .filter(
                and_(
                    UserQuery.user_id == user_id,
                    UserQuery.query == query,
                    UserQuery.date == func.current_date(),
                )
            )
            .first()
        )
        if user_query:
            user_query.count += 1
        else:
            user_query = UserQuery(user_id=user_id, query=query)
            self.session.add(user_query)

        user_statistic = (
            self.session.query(UserStatistic)
            .filter(UserStatistic.user_id == user_id)
            .first()
        )
        if user_statistic:
            user_statistic.total_queries += 1

        self.session.commit()

    def log_search_result_play(self, search_result_id: int):
        try:
            user_search_result = self._get_user_search_result(search_result_id)
        except exc.SearchResultNotFoundError:
            user_search_result = UserSearchResult(
                user=self.current_user,
                search_result_id=search_result_id,
                times_matched=1,
                times_played=0,
            )

        user_search_result.times_played += 1
        self.session.add(user_search_result)
        self.session.commit()

    def log_search_result_match(self, search_result_id: int):
        try:
            user_search_result = self._get_user_search_result(search_result_id)
        except exc.SearchResultNotFoundError:
            user_search_result = UserSearchResult(
                user=self.current_user,
                search_result_id=search_result_id,
                times_matched=0,
            )

        user_search_result.times_matched += 1
        self.session.add(user_search_result)
        self.session.commit()

    def _get_user_search_result(self, search_result_id: int):
        user_id = self.current_user.id
        user_search_result = (
            self.session.query(UserSearchResult)
            .filter(
                UserSearchResult.search_result_id == search_result_id,
                UserSearchResult.user_id == user_id,
            )
            .first()
        )

        if not user_search_result:
            raise exc.SearchResultNotFoundError(
                f"No matching search result with id: {search_result_id}"
                f"for user: {self.current_user.email}."
            )

        return user_search_result

    # Insights
    def get_most_popular_query_of_the_day(self):
        most_popular_query, n_count = (
            self.session.query(UserQuery.query, func.sum(UserQuery.count))
            .filter(UserQuery.date == func.current_date())
            .group_by(UserQuery.query)
            .order_by(func.sum(UserQuery.count).desc())
            .first()
        )

        return most_popular_query

    def count_video_views(self, video_id: int):
        return (
            self.session.query(func.sum(UserSearchResult.times_played))
            .join(SearchResult)
            .filter(SearchResult.video_id == video_id)
            .scalar()
        )

    def count_query_usage(self, query: str):
        return (
            self.session.query(func.sum(UserQuery.count))
            .filter(UserQuery.query == query)
            .scalar()
        )
