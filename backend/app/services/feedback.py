from sqlalchemy.orm import Session
from app.models.user import User, UserSearchResult, UserFavoriteContentSource
from app.schemas import UserInResponse


class FeedbackService:
    def __init__(self, session: Session, current_user: UserInResponse):
        self.session = session
        self.current_user = current_user

    def like_search_result(self, search_result_id: int):
        user = self.current_user
        if user:
            user_search_result = (
                self.session.query(UserSearchResult)
                .filter(
                    UserSearchResult.user_id == user.id,
                    UserSearchResult.search_result_id == search_result_id,
                )
                .first()
            )
            if user_search_result:
                user_search_result.is_liked = True
            else:
                user_search_result = UserSearchResult(
                    user_id=user.id, search_result_id=search_result_id, is_liked=True
                )
                self.session.add(user_search_result)
            self.session.commit()

    def unlike_search_result(self, search_result_id: int):
        user = self.current_user
        if user:
            user_search_result = (
                self.session.query(UserSearchResult)
                .filter(
                    UserSearchResult.user_id == user.id,
                    UserSearchResult.search_result_id == search_result_id,
                )
                .first()
            )
            if user_search_result:
                user_search_result.is_liked = False
                self.session.commit()

    def favorite_content_source(self, content_source_id: int):
        user = self.current_user
        if user:
            favorite_content_source = UserFavoriteContentSource(
                user_id=user.id, content_source_id=content_source_id
            )
            self.session.add(favorite_content_source)
            self.session.commit()

    def unfavorite_content_source(self, content_source_id: int):
        user = self.current_user
        if user:
            favorite_content_source = (
                self.session.query(UserFavoriteContentSource)
                .filter(
                    UserFavoriteContentSource.user_id == user.id,
                    UserFavoriteContentSource.content_source_id == content_source_id,
                )
                .first()
            )
            if favorite_content_source:
                self.session.delete(favorite_content_source)
                self.session.commit()
