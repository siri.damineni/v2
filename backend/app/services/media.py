from typing import Optional
from sqlalchemy.orm import Session

from app.models.media import Video, ClosedCaption, ClosedCaptionSegment
from app.schemas import YTSearchResult, YTTranscriptSegment
from app.services.youtube import YoutubeService
from app import exceptions as exc
from app.config import Languages


class MediaService:
    def __init__(self, session: Session, youtube_service: YoutubeService):
        self.session = session
        self.youtube_service = youtube_service

    def get_or_create_video(self, video_data: YTSearchResult) -> Video:
        video = (
            self.session.query(Video)
            .filter(Video.video_id == video_data.videoId)
            .first()
        )

        if not video:
            video = Video(
                video_id=video_data.videoId,
                title=video_data.title,
                thumbnail_url=video_data.thumbnailUrl,
            )
            self.session.add(video)

        return video

    def download_closed_caption(
        self, video: Video, language: Languages
    ) -> Optional[ClosedCaption]:
        cc_data = self.youtube_service.fetch_closed_captions(
            video.video_id, language.value
        )

        cc = ClosedCaption(video_id=video.video_id, language=language)
        self.session.add(cc)

        for segment_data in cc_data:
            segment = ClosedCaptionSegment(
                closed_caption_id=cc.id,
                start_time=segment_data.start,
                end_time=segment_data.start + segment_data.duration,
                text=segment_data.text,
            )
            self.session.add(segment)
            self.session.commit()
        return cc
