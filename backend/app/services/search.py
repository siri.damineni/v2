from typing import List
from sqlalchemy.orm import Session
from sqlalchemy_searchable import search

from app.models.media import ClosedCaptionSegment, ClosedCaption
from app.models.search import SearchResult
from app.services.youtube import YoutubeService
from app.services.cache import CacheService
from app.services.media import MediaService
from app.services.analytics import AnalyticsService
from app.config import settings, Languages


class SearchService:
    def __init__(
        self,
        session: Session,
        youtube_service: YoutubeService,
        media_service: MediaService,
        cache_service: CacheService,
        analytics_service: AnalyticsService,
    ):
        self.session = session
        self.youtube_service = youtube_service
        self.cache_service = cache_service
        self.media_service = media_service
        self.analytics_service = analytics_service

    def _get_matches_in_cc(
        self, query: str, cc: ClosedCaption
    ) -> List[ClosedCaptionSegment]:
        return [
            segment
            for segment in cc.closed_caption_segments
            if query.lower() in segment.text.lower()
        ]

    def get_cc_matches(self, query: str) -> List[ClosedCaptionSegment]:
        try:
            return search(self.session.query(ClosedCaptionSegment), query).all()
        except IndexError:
            return []

    def search_videos(self, query: str, language: str = "en") -> List[SearchResult]:
            
        def return_cached_results():
            # first, check if search results are already cached
            cached_results = self.cache_service.get_cached_results(
                query,
                max_results=settings.MAX_SEARCH_RESULTS,
            )

            return cached_results

        def search_in_existing_cc():
            # second, check if there are any matches in already downloaded CCs
            existing_cc_matches = self.get_cc_matches(query)
            if existing_cc_matches:
                for cc_match in existing_cc_matches:
                    self.cache_service.cache_search_result(
                        query=query,
                        cc_segment=cc_match,
                        video=cc_match.closed_caption.video,
                    )
            existing_cc_matches = self.cache_service.get_cached_results(
                query,
                max_results=settings.MAX_SEARCH_RESULTS,
            )

            return existing_cc_matches

        def search_on_youtube():
            # if cached and existing matches are not enough, query YouTube API
            video_results = self.youtube_service.search_videos(
                query, settings.MAX_YT_VIDEOS_RESULTS
            )
            for video_result in video_results:
                try:
                    video = self.media_service.get_or_create_video(video_result)
                    cc = self.media_service.download_closed_caption(
                        video, language=Languages.ENGLISH
                    )
                    # self.media_service.session.commit()  # batching txns

                    cc_matches = self._get_matches_in_cc(query, cc)
                    for cc_match in cc_matches:
                        self.cache_service.cache_search_result(
                            query=query,
                            cc_segment=cc_match,
                            video=cc_match.closed_caption.video,
                        )
                    # self.cache_service.session.commit()  # batching txns

                except Exception as e:
                    print(
                        f"Skipped processing https://youtu.be/{video.video_id}: {str(e)[:100]}"
                    )

            search_results = self.cache_service.get_cached_results(
                query,
                max_results=settings.MAX_SEARCH_RESULTS,
            )

            return search_results

        
        # if len(cached_results) + len(existing_cc_matches) < settings.MAX_SEARCH_RESULTS:
        search_results = return_cached_results()

        if len(search_results) < settings.MAX_SEARCH_RESULTS:
            search_results = search_in_existing_cc()

        if len(search_results) < settings.MAX_SEARCH_RESULTS:
            search_results = search_on_youtube()

        # log user search
        self.analytics_service.log_user_query(query=query)

        # log search result match
        for sr in search_results:
            self.analytics_service.log_search_result_match(search_result_id=sr.id)

        return search_results
