from typing import List
from sqlalchemy.orm import Session
from app.models.search import SearchResult
from app.models.media import ClosedCaptionSegment, Video
from app import schemas


class CacheService:
    def __init__(self, session: Session):
        self.session = session

    def get_cached_results(
        self, query: str, max_results: int
    ) -> List[schemas.SearchResult]:
        search_results = (
            self.session.query(SearchResult)
            .filter(SearchResult.query == query)
            .limit(max_results)
            .all()
        )
        return [schemas.SearchResult(**sr.to_dict()) for sr in search_results]

    def cache_search_result(
        self,
        query: str,
        cc_segment: ClosedCaptionSegment,
        video: Video,
    ) -> None:
        search_result = SearchResult(
            query=query,
            video=video,
            closed_caption_segment=cc_segment,
        )
        self.session.add(search_result)
        self.session.commit()
