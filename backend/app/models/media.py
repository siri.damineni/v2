from sqlalchemy_utils.types import TSVectorType
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from .base import Base
from ..config import Languages
from sqlalchemy.dialects.postgresql import ENUM as SQLEnum


class ContentSource(Base):
    __tablename__ = "content_sources"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True)

    video = relationship("Video", back_populates="content_source")
    user_favorite_content_sources = relationship(
        "UserFavoriteContentSource", back_populates="content_source"
    )


class Video(Base):
    __tablename__ = "videos"

    id = Column(Integer, primary_key=True, index=True)
    video_id = Column(String, unique=True, index=True)
    title = Column(String)
    thumbnail_url = Column(String)
    content_source_id = Column(Integer, ForeignKey("content_sources.id"))

    content_source = relationship("ContentSource", back_populates="video")
    closed_captions = relationship("ClosedCaption", back_populates="video")
    search_results = relationship("SearchResult", back_populates="video")


class ClosedCaption(Base):
    __tablename__ = "closed_captions"

    id = Column(Integer, primary_key=True, index=True)
    language = Column(SQLEnum(Languages))
    video_id = Column(String, ForeignKey("videos.video_id"))

    video = relationship("Video", back_populates="closed_captions")
    closed_caption_segments = relationship(
        "ClosedCaptionSegment", back_populates="closed_caption"
    )


class ClosedCaptionSegment(Base):
    __tablename__ = "closed_caption_segments"

    id = Column(Integer, primary_key=True, index=True)
    closed_caption_id = Column(Integer, ForeignKey("closed_captions.id"))
    start_time = Column(Float)
    end_time = Column(Float)
    text = Column(String, index=True)
    search_vector = Column(TSVectorType("text"))

    closed_caption = relationship(
        "ClosedCaption", back_populates="closed_caption_segments"
    )
    search_results = relationship(
        "SearchResult", back_populates="closed_caption_segment"
    )
