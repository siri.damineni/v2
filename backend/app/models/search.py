from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from .base import Base


class SearchResult(Base):
    __tablename__ = "search_results"

    id = Column(Integer, primary_key=True, index=True)
    query = Column(String, index=True)
    video_id = Column(Integer, ForeignKey("videos.id"))
    closed_caption_segment_id = Column(
        Integer, ForeignKey("closed_caption_segments.id")
    )

    video = relationship("Video", back_populates="search_results")
    closed_caption_segment = relationship(
        "ClosedCaptionSegment", back_populates="search_results"
    )
    user_search_results = relationship(
        "UserSearchResult", back_populates="search_result"
    )

    def to_dict(self):
        return {
            "id": self.id,
            "query": self.query,
            "video_id": self.video.video_id,
            "title": self.video.title,
            "thumbnail_url": self.video.thumbnail_url,
            "start_time": int(self.closed_caption_segment.start_time),
            "end_time": int(self.closed_caption_segment.end_time),
            "text": self.closed_caption_segment.text,
        }


class CustomResultsList(Base):
    __tablename__ = "custom_results_lists"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    name = Column(String)
    description = Column(String)
    popularity_score = Column(Integer, default=0)

    user = relationship("User", back_populates="custom_results_lists")
    entries = relationship(
        "CustomResultsListEntry", back_populates="custom_results_list"
    )


class CustomResultsListEntry(Base):
    __tablename__ = "custom_results_list_entries"

    id = Column(Integer, primary_key=True, index=True)
    custom_results_list_id = Column(Integer, ForeignKey("custom_results_lists.id"))
    search_result_id = Column(Integer, ForeignKey("search_results.id"))

    custom_results_list = relationship("CustomResultsList", back_populates="entries")
    search_result = relationship("SearchResult")
