from sqlalchemy import Column, Integer, String, Boolean, Date
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql import func
from .base import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)

    preferences = relationship("UserPreference", uselist=False, back_populates="user")
    statistics = relationship("UserStatistic", uselist=False, back_populates="user")
    favorite_content_sources = relationship(
        "UserFavoriteContentSource", back_populates="user"
    )
    followers = relationship(
        "UserFollow",
        foreign_keys="UserFollow.following_id",
        back_populates="follower",
    )
    followings = relationship(
        "UserFollow",
        foreign_keys="UserFollow.follower_id",
        back_populates="following",
    )
    blocked_users = relationship(
        "UserBlock",
        foreign_keys="UserBlock.blocker_id",
        back_populates="blocker",
    )
    blocking_users = relationship(
        "UserBlock",
        foreign_keys="UserBlock.blocked_id",
        back_populates="blocked",
    )
    user_queries = relationship("UserQuery", back_populates="user")
    custom_results_lists = relationship("CustomResultsList", back_populates="user")
    user_search_results = relationship("UserSearchResult", back_populates="user")


class UserPreference(Base):
    __tablename__ = "user_preferences"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    theme = Column(String)

    user = relationship("User", back_populates="preferences")


class UserStatistic(Base):
    __tablename__ = "user_statistics"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    total_queries = Column(Integer, default=0)
    words_learned = Column(Integer, default=0)
    videos_watched = Column(Integer, default=0)
    continuous_day_streak = Column(Integer, default=0)

    user = relationship("User", back_populates="statistics")


class UserQuery(Base):
    __tablename__ = "user_queries"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    query = Column(String, index=True)
    date = Column(Date, default=func.current_date())
    count = Column(Integer, default=1)

    user = relationship("User", back_populates="user_queries")


class UserSearchResult(Base):
    __tablename__ = "user_search_results"

    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    search_result_id = Column(
        Integer, ForeignKey("search_results.id"), primary_key=True
    )
    times_played = Column(Integer, default=0)
    times_matched = Column(Integer, default=0)
    is_liked = Column(Boolean, nullable=True)

    user = relationship("User", back_populates="user_search_results")
    search_result = relationship("SearchResult", back_populates="user_search_results")


class UserFavoriteContentSource(Base):
    __tablename__ = "user_favorite_content_sources"

    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    content_source_id = Column(
        Integer, ForeignKey("content_sources.id"), primary_key=True
    )

    user = relationship("User", back_populates="favorite_content_sources")
    content_source = relationship("ContentSource")


class UserFollow(Base):
    __tablename__ = "user_follows"

    id = Column(Integer, primary_key=True, index=True)
    follower_id = Column(Integer, ForeignKey("users.id"))
    following_id = Column(Integer, ForeignKey("users.id"))

    follower = relationship(
        "User", foreign_keys=[follower_id], back_populates="followings"
    )
    following = relationship(
        "User", foreign_keys=[following_id], back_populates="followers"
    )


class UserBlock(Base):
    __tablename__ = "user_blocks"

    id = Column(Integer, primary_key=True, index=True)
    blocker_id = Column(Integer, ForeignKey("users.id"))
    blocked_id = Column(Integer, ForeignKey("users.id"))

    blocker = relationship(
        "User", foreign_keys=[blocker_id], back_populates="blocked_users"
    )
    blocked = relationship(
        "User", foreign_keys=[blocked_id], back_populates="blocking_users"
    )
