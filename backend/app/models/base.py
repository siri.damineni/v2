from sqlalchemy.orm import declarative_base
from sqlalchemy_searchable import make_searchable

Base = declarative_base()

make_searchable(Base.metadata)
