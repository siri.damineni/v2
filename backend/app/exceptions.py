class AppBaseException(Exception):
    """Base exception for the app, all custom app exceptions should extend this."""


class UserNotFoundError(AppBaseException):
    """Exception raised when a user is not found."""


class InvalidCredentialsError(AppBaseException):
    """Exception raised when supplied credentials are invalid."""


class UserAlreadyExistsError(AppBaseException):
    """Exception raised when trying to register a user that already exists."""


class UnauthorizedError(AppBaseException):
    """Exception raised for unauthorized access attempts."""


class SearchResultNotFoundError(AppBaseException):
    """Exception raised when a requested search result does not exist."""


class ClosedCaptionDoesNotExistForVideo(AppBaseException):
    """Exception raised when a video doesn't have closed captions available for a given language"""


class ContentSourceNotFoundError(AppBaseException):
    """Exception raised when a requested content source does not exist."""


class BadRequestError(AppBaseException):
    """Exception raised when a bad request is made."""


class ServiceUnavailableError(AppBaseException):
    """Exception raised when a service is unavailable."""
