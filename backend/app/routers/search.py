from fastapi import APIRouter, Depends
from typing import List
from app.schemas import SearchResult, SearchHistory
from app.dependencies import get_search_service, get_user_service

router = APIRouter()


@router.get("/search", response_model=List[SearchResult])
def search_videos(query: str, search_service=Depends(get_search_service)):
    """
    Search videos using a provided query.
    """
    matches = search_service.search_videos(query)
    return matches


@router.get("/search/history", response_model=List[SearchHistory])
def get_search_history(user_service=Depends(get_user_service)):
    """
    Get the search history for the current user.
    """
    search_history = user_service.get_search_history()
    return search_history
