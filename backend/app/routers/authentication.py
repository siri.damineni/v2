import app.exceptions as exc
from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from app.schemas import UserInCreate, Token
from app.dependencies import get_auth_service
from app.services.authentication import AuthService

router = APIRouter()


@router.post("/login", response_model=Token)
def login(
    form_data: OAuth2PasswordRequestForm = Depends(),
    auth_service: AuthService = Depends(get_auth_service),
) -> Token:
    """
    Login an existing user
    """
    try:
        return auth_service.authenticate_user(form_data.username, form_data.password)
    except exc.InvalidCredentialsError:
        raise HTTPException(
            status_code=400,
            detail="Invalid credentials",
        )
    except exc.UserNotFoundError:
        raise HTTPException(
            status_code=404,
            detail="User not found",
        )


@router.post("/register", response_model=Token)
def register_user(
    user: UserInCreate, auth_service: AuthService = Depends(get_auth_service)
) -> Token:
    """
    Register a new user
    """
    try:
        return auth_service.create_user(user)
    except exc.UserAlreadyExistsError:
        raise HTTPException(
            status_code=400,
            detail="User already exists",
        )
