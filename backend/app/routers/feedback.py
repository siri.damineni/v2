from fastapi import APIRouter, Depends, HTTPException, status
from app.dependencies import get_feedback_service

router = APIRouter()


@router.put("/search/results/{search_result_id}/like-status")
def like_search_result(
    search_result_id: int, feedback_service=Depends(get_feedback_service)
):
    """
    Like a specific search result
    """
    try:
        feedback_service.like_search_result(search_result_id)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    return {"message": "Successfully liked the search result."}


@router.delete("/search/results/{search_result_id}/like-status")
def unlike_search_result(
    search_result_id: int, feedback_service=Depends(get_feedback_service)
):
    """
    Unlike a specific search result
    """
    try:
        feedback_service.unlike_search_result(search_result_id)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    return {"message": "Successfully unliked the search result."}


@router.put("/content-sources/{content_source_id}/favorite-status")
def favorite_content_source(
    content_source_id: int, feedback_service=Depends(get_feedback_service)
):
    """
    Favorite a specific content source
    """
    try:
        feedback_service.favorite_content_source(content_source_id)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    return {"message": "Successfully favorited the content source."}


@router.delete("/content-sources/{content_source_id}/favorite-status")
def unfavorite_content_source(
    content_source_id: int, feedback_service=Depends(get_feedback_service)
):
    """
    Unfavorite a specific content source
    """
    try:
        feedback_service.unfavorite_content_source(content_source_id)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    return {"message": "Successfully unfavorited the content source."}


@router.get("/content-sources")
def get_favorite_content_sources(feedback_service=Depends(get_feedback_service)):
    """
    Get favorite content sources for the current user
    """
    try:
        favorite_sources = feedback_service.get_favorite_content_sources()
        return favorite_sources
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
