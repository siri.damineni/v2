from fastapi import APIRouter, Depends, HTTPException, status
from app.dependencies import get_analytics_service

router = APIRouter()


@router.post("/search/results/{search_result_id}/play")
def log_search_result_play(
    search_result_id: int, analytics_service=Depends(get_analytics_service)
):
    """
    Log when a search result is played
    """
    try:
        analytics_service.log_search_result_play(search_result_id)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    return {"message": "Successfully logged play of the search result."}


@router.post("/search/results/{search_result_id}/match")
def log_search_result_match(
    search_result_id: int, analytics_service=Depends(get_analytics_service)
):
    """
    Log when a search result matches a user query
    """
    try:
        analytics_service.log_search_result_match(search_result_id)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    return {"message": "Successfully logged match of the search result."}


@router.get("/analytics/queries/popular")
def get_most_popular_query_of_the_day(analytics_service=Depends(get_analytics_service)):
    """
    Get the most popular query of the day
    """
    try:
        return analytics_service.get_most_popular_query_of_the_day()
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
