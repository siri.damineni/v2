from fastapi import APIRouter, Depends, HTTPException
from typing import List

from .. import schemas
from ..services.user import UserService
from ..dependencies import get_user_service

router = APIRouter()


@router.get("/users/me", response_model=schemas.UserInResponse)
def read_user_me(
    user_service: UserService = Depends(get_user_service),
):
    try:
        current_user = user_service.current_user
    except Exception as e:
        raise HTTPException(status_code=404, detail=str(e))

    return current_user


# @router.put("/users/me", response_model=schemas.UserInResponse)
# def update_user_me(
#     user_update: schemas.UserUpdate,
#     user_service: UserService = Depends(get_user_service),
# ):
#     """
#     Update the current user's details
#     """
#     updated_user = user_service.update_current_user(user_update)
#     return updated_user


@router.post("/users/me/followed/{followed_user_id}")
def follow_user(
    followed_user_id: int, user_service: UserService = Depends(get_user_service)
):
    user_service.follow_user(followed_user_id)
    return {"detail": f"Successfully followed user {followed_user_id}"}


@router.delete("/users/me/followed/{followed_user_id}")
def unfollow_user(
    followed_user_id: int, user_service: UserService = Depends(get_user_service)
):
    user_service.unfollow_user(followed_user_id)
    return {"detail": f"Successfully unfollowed user {followed_user_id}"}


@router.post("/users/me/blocked/{blocked_user_id}")
def block_user(
    blocked_user_id: int, user_service: UserService = Depends(get_user_service)
):
    user_service.block_user(blocked_user_id)
    return {"detail": f"Successfully blocked user {blocked_user_id}"}


@router.delete("/users/me/blocked/{blocked_user_id}")
def unblock_user(
    blocked_user_id: int, user_service: UserService = Depends(get_user_service)
):
    user_service.unblock_user(blocked_user_id)
    return {"detail": f"Successfully unblocked user {blocked_user_id}"}


@router.get("/users/me/statistics")
def get_user_statistics(user_service: UserService = Depends(get_user_service)):
    return user_service.get_user_statistics()


@router.get("/users/me/preferences")
def get_user_preferences(user_service: UserService = Depends(get_user_service)):
    return user_service.get_user_preferences()


@router.put("/users/me/preferences")
def update_user_preferences(user_service: UserService = Depends(get_user_service)):
    return user_service.update_user_preferences()


@router.get("/users/me/followers", response_model=List[schemas.UserInResponse])
def get_my_followers(user_service: UserService = Depends(get_user_service)):
    return user_service.get_user_followers()


@router.get("/users/me/followed", response_model=List[schemas.UserInResponse])
def get_people_i_follow(user_service: UserService = Depends(get_user_service)):
    return user_service.get_user_followings()


@router.get("/users/me/blocked", response_model=List[schemas.UserInResponse])
def get_blocked_users(user_service: UserService = Depends(get_user_service)):
    return user_service.get_user_blocked_users()
