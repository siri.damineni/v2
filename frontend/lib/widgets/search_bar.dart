import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmitted;
  final VoidCallback onSubmit;

  const SearchBar({
    Key? key,
    required this.onChanged,
    required this.onSubmitted,
    required this.onSubmit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      onSubmitted: onSubmitted,
      decoration: InputDecoration(
        labelText: 'Search',
        hintText: 'Enter a word to learn',
        suffixIcon: IconButton(
          icon: Icon(Icons.search),
          onPressed: onSubmit,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
