import 'package:flutter/material.dart';
import 'package:frontend/services/youtube_service.dart';
import 'package:frontend/widgets/feedback_buttons.dart';

class VideoResultsTile extends StatelessWidget {
  final VideoResult videoResult;
  final VoidCallback onUpvote;
  final VoidCallback onDownvote;
  final VoidCallback onTap;
  final bool isLiked;
  final bool isDisliked;

  const VideoResultsTile({
    Key? key,
    required this.videoResult,
    required this.onUpvote,
    required this.onDownvote,
    required this.onTap,
    required this.isLiked,
    required this.isDisliked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        child: Column(
          children: [
            ListTile(
              leading: Image.network(videoResult.thumbnailUrl),
              title: Text(videoResult.title),
            ),
            FeedbackButtons(
              onUpvote: onUpvote,
              onDownvote: onDownvote,
              isLiked: isLiked,
              isDisliked: isDisliked,
            ),
          ],
        ),
      ),
    );
  }
}
