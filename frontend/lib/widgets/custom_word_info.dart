import 'package:flutter/material.dart';

class WordInfo extends StatelessWidget {
  final String definition;
  final String etymology;

  const WordInfo({
    Key? key,
    required this.definition,
    required this.etymology,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('Definition:',
            style: TextStyle(fontWeight: FontWeight.bold)),
        Text(definition),
        SizedBox(height: 8),
        const Text('Etymology:', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(etymology),
      ],
    );
  }
}
