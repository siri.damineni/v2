import 'package:flutter/material.dart';

class FeedbackButtons extends StatelessWidget {
  final VoidCallback onUpvote;
  final VoidCallback onDownvote;
  final bool isLiked;
  final bool isDisliked;

  const FeedbackButtons({
    Key? key,
    required this.onUpvote,
    required this.onDownvote,
    required this.isLiked,
    required this.isDisliked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(
            isLiked ? Icons.thumb_up_alt_rounded : Icons.thumb_up_outlined,
            color: isLiked ? Colors.green : null,
          ),
          onPressed: onUpvote,
        ),
        const SizedBox(width: 16),
        IconButton(
          icon: Icon(
            isDisliked
                ? Icons.thumb_down_alt_rounded
                : Icons.thumb_down_outlined,
            color: isDisliked ? Colors.red : null,
          ),
          onPressed: onDownvote,
        ),
      ],
    );
  }
}
