import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:frontend/config.dart';
import 'package:frontend/services/secure_storage.dart';

class AuthService {
  static final SecureStorage secureStorage = SecureStorage();

  static Future<bool> register(String email, String password) async {
    final response = await http.post(
      Uri.parse('${Config.baseUrl}/api/register'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: jsonEncode({'email': email, 'password': password}),
    );

    if (response.statusCode == 200) {
      // Handle successful registration
      return true;
    } else {
      // Handle registration error
      return false;
    }
  }

  static Future<bool> login(String email, String password) async {
    final response = await http.post(
      Uri.parse('${Config.baseUrl}/api/login'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
      },
      body: {
        'username': email,
        'password': password,
        'grant_type': 'password',
      },
    );

    if (response.statusCode == 200) {
      // Store the access token and handle successful login
      // For example, you can use shared_preferences to store the access token
      final data = jsonDecode(response.body);
      await secureStorage.setAccessToken(data['access_token']);
      // Save the accessToken and return true for successful login
      return true;
    } else {
      // Handle login error
      return false;
    }
  }

  static Future<void> logout() async {
    // Remove the access token from storage and perform any cleanup needed
    await secureStorage.deleteAccessToken();
  }
}
