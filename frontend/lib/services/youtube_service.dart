import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:frontend/services/secure_storage.dart';
import 'package:frontend/config.dart';

class VideoResult {
  final String videoId;
  final String title;
  final String thumbnailUrl;
  final int startTime;
  final int endTime;

  VideoResult({
    required this.videoId,
    required this.title,
    required this.thumbnailUrl,
    required this.startTime,
    required this.endTime,
  });

  factory VideoResult.fromJson(Map<String, dynamic> json) {
    return VideoResult(
      videoId: json['video_id'],
      title: json['title'],
      thumbnailUrl: json['thumbnail_url'],
      startTime: json['start_time'],
      endTime: json['end_time'],
    );
  }
}

class YoutubeService {
  static final SecureStorage secureStorage = SecureStorage();

  static Future<List<VideoResult>> searchVideos(String query) async {
    String? accessToken = await secureStorage.getAccessToken();

    final response = await http.get(
      Uri.parse('${Config.baseUrl}/api/search?query=$query'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
      },
    );

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      return data
          .map<VideoResult>((item) => VideoResult.fromJson(item))
          .toList();
    } else {
      // Handle search error
      return [];
    }
  }
}
