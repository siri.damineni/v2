import 'package:flutter/material.dart';
import 'package:frontend/services/youtube_service.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String _searchQuery = '';
  List<VideoResult> _searchResults = [];
  List<VideoResult> _likedResults = [];
  List<VideoResult> _dislikedResults = [];
  bool _isLoading = false;
  bool _showVideoPane = false;
  String? _videoId;
  int? _startTime;
  int? _endTime;

  Future<void> _search() async {
    final results = await YoutubeService.searchVideos(_searchQuery);
    setState(() {
      _searchResults = results;
      _isLoading = false;
    });
  }

  Future<void> _submit() async {
    if (_searchQuery.isNotEmpty) {
      setState(() {
        _isLoading = true;
      });
      await _search();
    }
  }

  void _showVideo(String videoId, int startTime, int endTime) {
    setState(() {
      _showVideoPane = true;
      _videoId = videoId;
      _startTime = startTime;
      _endTime = endTime;
    });
  }

  void _hideVideo() {
    setState(() {
      _showVideoPane = false;
    });
  }

  void _likeVideo(VideoResult videoResult) {
    setState(() {
      _likedResults.add(videoResult);
    });
  }

  void _dislikeVideo(VideoResult videoResult) {
    setState(() {
      _dislikedResults.add(videoResult);
    });
  }

  bool _isVideoLiked(VideoResult video) {
    return _likedResults.any((likedVideo) =>
        likedVideo.videoId == video.videoId &&
        likedVideo.startTime == video.startTime &&
        likedVideo.endTime == video.endTime);
  }

  bool _isVideoDisliked(VideoResult video) {
    return _dislikedResults.any((dislikedVideo) =>
        dislikedVideo.videoId == video.videoId &&
        dislikedVideo.startTime == video.startTime &&
        dislikedVideo.endTime == video.endTime);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cliptionary'),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              // Handle logout and navigate to the login screen
            },
          ),
        ],
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onChanged: (value) => _searchQuery = value,
                  onSubmitted: (value) => _submit(),
                  decoration: InputDecoration(
                    labelText: 'Search',
                    hintText: 'Enter a word to learn',
                    suffixIcon: IconButton(
                      icon: Icon(Icons.search),
                      onPressed: _submit,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: _isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : _searchResults.isNotEmpty
                        ? ListView.builder(
                            itemCount: _searchResults.length,
                            itemBuilder: (context, index) {
                              final videoResult = _searchResults[index];
                              return ListTile(
                                leading:
                                    Image.network(videoResult.thumbnailUrl),
                                title: Text(videoResult.title),
                                onTap: () async {
                                  final isLiked = _isVideoLiked(videoResult);
                                  final isDisliked =
                                      _isVideoDisliked(videoResult);
                                  if (!_showVideoPane) {
                                    setState(() {
                                      _showVideoPane = true;
                                      _videoId = videoResult.videoId;
                                      _startTime = videoResult.startTime;
                                      _endTime = videoResult.endTime;
                                    });
                                  }
                                },
                                trailing: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        _isVideoLiked(videoResult)
                                            ? Icons.thumb_up_alt_rounded
                                            : Icons.thumb_up_outlined,
                                      ),
                                      onPressed: () async {
                                        if (_isVideoLiked(videoResult)) {
                                          setState(() {
                                            _likedResults.remove(videoResult);
                                          });
                                        } else {
                                          setState(() {
                                            _likedResults.add(videoResult);
                                          });
                                        }
                                      },
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        _isVideoDisliked(videoResult)
                                            ? Icons.thumb_down_alt_rounded
                                            : Icons.thumb_down_outlined,
                                      ),
                                      onPressed: () async {
                                        if (_isVideoDisliked(videoResult)) {
                                          setState(() {
                                            _dislikedResults
                                                .remove(videoResult);
                                          });
                                        } else {
                                          setState(() {
                                            _dislikedResults.add(videoResult);
                                          });
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                          )
                        : const Center(
                            child:
                                Text('No results found, search another word.'),
                          ),
              ),
            ],
          ),
          if (_showVideoPane)
            VideoPane(
              videoId: _videoId!,
              startTime: _startTime!,
              endTime: _endTime!,
              onClose: _hideVideo,
            ),
        ],
      ),
    );
  }
}

class VideoPane extends StatefulWidget {
  final String videoId;
  final int startTime;
  final int endTime;
  final VoidCallback onClose;

  VideoPane({
    Key? key,
    required this.videoId,
    required this.startTime,
    required this.endTime,
    required this.onClose,
  }) : super(key: key);

  @override
  _VideoPaneState createState() => _VideoPaneState();
}

class _VideoPaneState extends State<VideoPane> {
  late YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
        initialVideoId: widget.videoId,
        params: YoutubePlayerParams(
          startAt: Duration(seconds: widget.startTime),
          // endAt: Duration(seconds: widget.endTime),
          autoPlay: true,
          showControls: false,
          showFullscreenButton: false,
        ))
      ..listen((value) {
        if (value.position.inSeconds >= widget.endTime) {
          _controller.pause();
          widget.onClose();
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: Container(
        color: Colors.black.withOpacity(0.7),
        child: Center(
          child: YoutubePlayerIFrame(
            controller: _controller,
            aspectRatio: 16 / 9,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}

class FeedbackPanel extends StatefulWidget {
  final VoidCallback onUpvote;
  final VoidCallback onDownvote;

  const FeedbackPanel({
    Key? key,
    required this.onUpvote,
    required this.onDownvote,
  }) : super(key: key);

  @override
  _FeedbackPanelState createState() => _FeedbackPanelState();
}

class _FeedbackPanelState extends State<FeedbackPanel> {
  bool _upvoted = false;
  bool _downvoted = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.thumb_up,
              color: _upvoted ? Colors.green : Colors.grey),
          onPressed: () {
            setState(() {
              _upvoted = !_upvoted;
              _downvoted = false;
            });
            widget.onUpvote();
          },
        ),
        const SizedBox(width: 16),
        IconButton(
          icon: Icon(Icons.thumb_down,
              color: _downvoted ? Colors.red : Colors.grey),
          onPressed: () {
            setState(() {
              _downvoted = !_downvoted;
              _upvoted = false;
            });
            widget.onDownvote();
          },
        ),
      ],
    );
  }
}
